Name:           libsx
Summary:        Simple X library
Version:        2.05
Release:        16%{?dist}
Group:          System Environment/Libraries
License:        LGPLv2+
Url:            ftp://ftp.ac-grenoble.fr/ge/Xlibraries/
Source:         ftp://ftp.ac-grenoble.fr/ge/Xlibraries/%{name}-%{version}.tar.bz2
# simpler example for freq
Source1:        libsx-simple_freq.c
Patch0:         libsx-no_nested_prototypes.diff
Patch1:         libsx-comment_caddr_t.diff
Patch2:         libsx-strdup.diff
Patch3:         libsx-examples.diff
Patch4:         libsx-link_mkdir.diff
Patch6:         libsx-rgb.diff
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
# libXt-devel requires libX11-devel and libXaw-devel requires libXmu-devel
BuildRequires:  libXaw-devel libXpm-devel libXt-devel 

%description
Libsx (the Simple X library) is a library of code that sits on top of and 
to the side of the Athena widget set.  Its purpose is to make writing X 
applications *much* easier.  

%package devel
Summary:       Headers and development libraries for libsx
Group:         Development/Libraries
Requires:      %{name} = %{version}-%{release}
#Requires:      xorg-x11-devel
Requires:      libXaw-devel libXpm-devel libXt-devel 

%description devel
Libsx (the Simple X library) is a library of code that sits on top of and
to the side of the Athena widget set.

The libsx-devel package contains headers and development libraries
necessary for building programs against libsx.


%prep
%setup -q
# static function cannot be within other function
%patch0 -p1
# don't redefine caddr_t
%patch1
# set strdup prototypes only if strdup isn't a macro
%patch2 -p1 -b .strdup
# use the new GetFile in freq, remove libsx.h from prerequisite and other
# fixes allowing examples to compile
%patch3 -p1 -b .examples
# use mkdir -p to create directories
# shared library with fine soname
# fix compile flags
%patch4 -p1 -b .link_mkdir
# allow the rgb file location to be redefined
%patch6 -p1 -b .rgb

# example of a simple use of the new GetFile
cp -p %{SOURCE1} freq/simple_freq.c

%build
make CFLAGS="%{optflags} -fPIC -DRGBTXT=\"\\\"%{_datadir}/X11/rgb.txt\\\"\""

%install
rm -rf %{buildroot}

pushd src
make install LIBDIR=%{buildroot}%{_libdir} \
      INCLUDEDIR=%{buildroot}%{_includedir} \
      SHAREDIR=%{buildroot}%{_datadir}/libsx \
      copyfile='cp -p'
popd
rm %{buildroot}%{_libdir}/libsx.a

# prepare examples directory
rm -rf __dist_examples
mkdir -p __dist_examples/examples
# pcurve doesn't build since it requires OPENGL_SUPPORT to be used, 
# and we don't use it, since it is maked as experimental.
example_dirs="bezier controlbox creq demo* draw_demo frac freq multireq skel xmore xrootbg" 
cp -a $example_dirs libsx_defs pcurve __dist_examples/examples/
# remove symlinks pointing to libsx.h in example directories
find __dist_examples/examples/ -name libsx.h -a -type l -exec rm \{\} \;
find __dist_examples/examples/ -name makefile.examples -exec rm \{\} \;
rm __dist_examples/examples/freq/freq.c.strdup
for dir in $example_dirs; do
  make -C __dist_examples/examples/$dir clean
done


# fix symbolic links for shared library. It is not completly obvious
# that using 0.0.0 like in libtool makes sense, do it anyway.
pushd %{buildroot}/%{_libdir}
mv libsx.so libsx.so.0.0.0
chmod +x libsx.so.0.0.0
ln -s libsx.so.0.0.0 libsx.so.0
ln -s libsx.so.0 libsx.so
popd

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc CHANGES HELP HINTS LICENSE README 
%{_libdir}/libsx.so.*
%{_datadir}/libsx/

%files devel
%defattr(-,root,root,-)
%doc docs/ __dist_examples/examples/
%{_libdir}/libsx.so
%{_includedir}/libsx.h

%changelog
* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.05-16
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 2.05-15
- Autorebuild for GCC 4.3

* Thu Dec 27 2007 Patrice Dumas <pertusus@free.fr> 2.05-14
- remove static lib

* Thu Dec 27 2007 Patrice Dumas <pertusus@free.fr> 2.05-13
- keep timestamps

* Thu Feb 15 2007 Patrice Dumas <pertusus@free.fr> 2.05-12
- build examples as part of building the library
- replace libsx-protect_strdup.diff with libsx-strdup.diff, which has the fix
  for freq/freq.c, not only for file in src/
- merge libsx-shared.diff and libsx-mkdir_p.diff in libsx-link_mkdir.diff

* Thu Feb 15 2007 Patrice Dumas <pertusus@free.fr> 2.05-11
- use only %%{buildroot}
- put examples in a directory and fix them
- set the rgb database location to a correct value
- replace libsx-freq_simple.diff by libsx-examples.diff + libsx-simple_freq.c

* Sun Sep 10 2006 Patrice Dumas <pertusus@free.fr> 2.05-10
- add defattr to devel

* Sat Jul 15 2006 Patrice Dumas <pertusus@free.fr> 2.05-9
- fix wrong link
- link against X libraries

* Fri Feb 17 2006 Patrice Dumas <pertusus@free.fr> 2.05-8
- rebuild for fc5

* Thu Dec 22 2005 Patrice Dumas <pertusus@free.fr> 2.05-7
- rebuild

* Wed Nov 16 2005 Patrice Dumas <pertusus@free.fr> 2.05-6
- adapt for modular X

* Fri Aug 26 2005 Patrice Dumas <pertusus@free.fr> 2.05-5
- replace mkinstalldirs with mkdir -p

* Fri Aug 26 2005 Patrice Dumas <pertusus@free.fr> 2.05-4
- correct Requires
- remove links to the libsx.h header files in the example directories

* Tue Aug 16 2005 Patrice Dumas <pertusus@free.fr> 2.05-3
- don't ship freq.h or libfreq.a, it is an example
- add a simpler definition of SimpleGetFile in freq
- ship the example dirs and the doc
- use and ship mkinstalldirs to create the dirs instead of mkdirhier
- handle correctly shared libraries

* Fri Aug 12 2005 Patrice Dumas <pertusus@free.fr> 2.05-2
- use %%{optflags} and build on fedora with gcc4
- reused spec from http://monkeyrpms.net/fc2-i386/html/libsx.html
